<?php

namespace Plastyk\PhpCsFixerConfig;

use PhpCsFixer\Config as PhpCsFixerConfig;

class Config extends PhpCsFixerConfig
{
    public function __construct()
    {
        parent::__construct('Plastyk PhpCsFixerConfig');

        $this->setLineEnding("\n");
        $this->setRules([
            '@PSR2' => true, // since 2.0.0
            '@PHP82Migration' => true, // since 3.14.4
            '@PHP80Migration:risky' => true, // since 2.15.9
            'align_multiline_comment' => [
                'comment_type' => 'all_multiline',
            ],
            'array_indentation' => true,
            'array_syntax' => [
                'syntax' => 'short',
            ],
            'binary_operator_spaces' => [
                'default' => 'single_space',
            ],
            'blank_line_after_opening_tag' => true,
            'blank_line_before_statement' => [
                'statements' => ['break', 'continue', 'declare', 'return', 'throw', 'try'],
            ],
            'cast_spaces' => [
                'space' => 'none',
            ],
            'concat_space' => [
                'spacing' => 'one',
            ],
            'explicit_string_variable' => true,
            'function_typehint_space' => true,
            'include' => true,
            'lowercase_cast' => true,
            'magic_constant_casing' => true,
            'multiline_whitespace_before_semicolons' => [
                'strategy' => 'no_multi_line',
            ],
            'native_function_casing' => true,
            'new_with_braces' => true,
            'no_blank_lines_after_class_opening' => true,
            'no_blank_lines_after_phpdoc' => true,
            'no_empty_comment' => true,
            'no_empty_phpdoc' => true,
            'no_empty_statement' => true,
            'no_leading_namespace_whitespace' => true,
            'no_mixed_echo_print' => [
                'use' => 'echo',
            ],
            'no_multiline_whitespace_around_double_arrow' => true,
            'no_singleline_whitespace_before_semicolons' => true,
            'no_spaces_around_offset' => [
                'positions' => ['inside', 'outside'],
            ],
            'no_trailing_comma_in_list_call' => true,
            'no_trailing_comma_in_singleline_array' => true,
            'no_unneeded_control_parentheses' => [
                'statements' => ['break', 'clone', 'continue', 'echo_print', 'return', 'switch_case', 'yield'],
            ],
            'no_unused_imports' => true,
            'no_useless_else' => true,
            'no_useless_return' => true,
            'no_whitespace_before_comma_in_array' => true,
            'no_whitespace_in_blank_line' => true,
            'normalize_index_brace' => true,
            'object_operator_without_whitespace' => true,
            'ordered_imports' => [
                'sort_algorithm' => 'alpha',
            ],
            'php_unit_fqcn_annotation' => true,
            'phpdoc_add_missing_param_annotation' => [
                'only_untyped' => true,
            ],
            'phpdoc_align' => [
                'tags' => ['param', 'return', 'throws', 'type', 'var'],
                'align' => 'left',
            ],
            'phpdoc_annotation_without_dot' => true,
            'phpdoc_indent' => true,
            'phpdoc_inline_tag_normalizer' => true,
            'phpdoc_no_alias_tag' => [
                'replacements' => ['property-read' => 'property', 'property-write' => 'property', 'type' => 'var', 'link' => 'see'],
            ],
            'phpdoc_no_empty_return' => true,
            'phpdoc_no_useless_inheritdoc' => true,
            'phpdoc_order' => true,
            'phpdoc_return_self_reference' => [
                'replacements' => ['this' => '$this', '@this' => '$this', '$self' => 'self', '@self' => 'self', '$static' => 'static', '@static' => 'static'],
            ],
            'phpdoc_scalar' => true,
            'phpdoc_separation' => false,
            'phpdoc_single_line_var_spacing' => true,
            'phpdoc_to_comment' => true,
            'phpdoc_trim' => true,
            'phpdoc_types' => true,
            'return_type_declaration' => [
                'space_before' => 'none',
            ],
            'semicolon_after_instruction' => true,
            'short_scalar_cast' => true,
            'simple_to_complex_string_variable' => true,
            'simplified_null_return' => true,
            'single_blank_line_before_namespace' => true,
            'single_line_comment_style' => [
                'comment_types' => ['hash'],
            ],
            'single_quote' => true,
            'space_after_semicolon' => [
                'remove_in_empty_for_expressions' => true,
            ],
            'standardize_not_equals' => true,
            'ternary_operator_spaces' => true,
            'trailing_comma_in_multiline' => [
                'elements' => ['arrays', 'arguments', 'parameters'],
            ],
            'trim_array_spaces' => true,
            'unary_operator_spaces' => true,
            'void_return' => false,
            'whitespace_after_comma_in_array' => true,
        ]);
    }
}
