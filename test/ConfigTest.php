<?php

namespace Plastyk\PhpCsFixerConfig\Test;

use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{
    /**
     * @test
     * @covers \Plastyk\PhpCsFixerConfig\Config
     */
    public function config_has_rules()
    {
        $config = new \Plastyk\PhpCsFixerConfig\Config();
        $this->assertNotEmpty($config->getRules());
    }
}
