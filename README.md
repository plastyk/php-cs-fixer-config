To use this package

```bash
composer config repositories.plastyk composer https://gitlab.com/api/v4/group/plastyk/-/packages/composer/
composer require --dev plastyk/php-cs-fixer-config
```

add a config file `.php-cs-fixer.php` in the root of your project with the following content:

```php
<?php

$fixer = new \Plastyk\PhpCsFixerConfig\Config();

$fixer
    ->getFinder()
        ->exclude(['vendor', 'node_modules']);

return $fixer;
```
