<?php

$config = new \Plastyk\PhpCsFixerConfig\Config();
$config->getFinder()
    ->in(__DIR__ . '/src')
    ->in(__DIR__ . '/test');

return $config;
